################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/lib/dsp.c \
../Core/Src/lib/fatfs_sd.c \
../Core/Src/lib/fonts.c \
../Core/Src/lib/menu.c \
../Core/Src/lib/ssd1306.c \
../Core/Src/lib/transfer_data.c 

OBJS += \
./Core/Src/lib/dsp.o \
./Core/Src/lib/fatfs_sd.o \
./Core/Src/lib/fonts.o \
./Core/Src/lib/menu.o \
./Core/Src/lib/ssd1306.o \
./Core/Src/lib/transfer_data.o 

C_DEPS += \
./Core/Src/lib/dsp.d \
./Core/Src/lib/fatfs_sd.d \
./Core/Src/lib/fonts.d \
./Core/Src/lib/menu.d \
./Core/Src/lib/ssd1306.d \
./Core/Src/lib/transfer_data.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/lib/%.o Core/Src/lib/%.su Core/Src/lib/%.cyclo: ../Core/Src/lib/%.c Core/Src/lib/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3 -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-lib

clean-Core-2f-Src-2f-lib:
	-$(RM) ./Core/Src/lib/dsp.cyclo ./Core/Src/lib/dsp.d ./Core/Src/lib/dsp.o ./Core/Src/lib/dsp.su ./Core/Src/lib/fatfs_sd.cyclo ./Core/Src/lib/fatfs_sd.d ./Core/Src/lib/fatfs_sd.o ./Core/Src/lib/fatfs_sd.su ./Core/Src/lib/fonts.cyclo ./Core/Src/lib/fonts.d ./Core/Src/lib/fonts.o ./Core/Src/lib/fonts.su ./Core/Src/lib/menu.cyclo ./Core/Src/lib/menu.d ./Core/Src/lib/menu.o ./Core/Src/lib/menu.su ./Core/Src/lib/ssd1306.cyclo ./Core/Src/lib/ssd1306.d ./Core/Src/lib/ssd1306.o ./Core/Src/lib/ssd1306.su ./Core/Src/lib/transfer_data.cyclo ./Core/Src/lib/transfer_data.d ./Core/Src/lib/transfer_data.o ./Core/Src/lib/transfer_data.su

.PHONY: clean-Core-2f-Src-2f-lib

