/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "fatfs.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lib/fonts.h"
#include "lib/ssd1306.h"
#include "lib/menu.h"
#include "lib/dsp.h"
#include "lib/fatfs_sd.h"
#include "lib/transfer_data.h"
#include "stdio.h"
#include "stdlib.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define Debounce (current_millis - previous_millis > 2)

#define N_SEND 20
#define N_BUFFER_ESP 20
/*					oled__flag					*/
#define SET_FLAG (uint8_t) 0x01


/*					global_flag					*/
#define ADC_FLAG (uint8_t) 0x00
#define SEND_FLAG (uint8_t) 0x01
#define SAVESD_FLAG (uint8_t) 0x02
#define OPENSD_FLAG (uint8_t) 0x04
#define BUFFER_1_FLAG (uint8_t) 0x08

#define CLEAR_FLAG (uint8_t) (~0xFF)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;
DMA_HandleTypeDef hdma_i2c1_tx;

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_tx;

osThreadId ADCHandle;
osThreadId SDHandle;
osMutexId MutexHandle;
/* USER CODE BEGIN PV */

//Store data to SD Card
uint16_t global_adc[N_SEND];

//Sleep counter
uint16_t sleep_count = 0;


//Send data UART ESP8266

char esp_send_buffer_1[N_BUFFER_ESP*3] = {0};
char esp_send_buffer_2[N_BUFFER_ESP*3] = {0};

//Previous and current millis for debounce switching
volatile uint32_t previous_millis = 0;
volatile uint32_t current_millis = 0;

//Navigate pages: 3 pages: main, chose, function
volatile uint8_t global_flag = 0;
volatile uint8_t esp_flag = 0;
volatile uint8_t oled_flag = SET_FLAG;
volatile uint8_t navigate = 0;
void (*menu_pointing[])(struct MenuPosition *) = {menu_start, menu_display, menu_choice};

struct ecg_dsp ecg_data;

/*			File descriptor 		*/
//FATFS fs;
//FIL fil;
//FRESULT fres;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_SPI1_Init(void);
void StartADC(void const * argument);
void StartSD(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  MX_SPI1_Init();
  MX_FATFS_Init();
  /* USER CODE BEGIN 2 */

  //HAL_SPI_MspInit(&hspi1);
  //HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
  //HAL_Delay(1000);
  //HAL_SPI_MspDeInit(&hspi1);

  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of Mutex */
  osMutexDef(Mutex);
  MutexHandle = osMutexCreate(osMutex(Mutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of ADC */
  osThreadDef(ADC, StartADC, osPriorityRealtime, 0, 128);
  ADCHandle = osThreadCreate(osThread(ADC), NULL);

  /* definition and creation of SD */
  osThreadDef(SD, StartSD, osPriorityAboveNormal, 0, 512);
  SDHandle = osThreadCreate(osThread(SD), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PB13 PB14 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

//Interrupt and other functions
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	//Count time for debounce
	sleep_count = 0;
	HAL_ResumeTick();
	current_millis = HAL_GetTick();
	oled_flag |= SET_FLAG;
	if(GPIO_Pin == GPIO_PIN_14 && Debounce){
		//Choose Function in Menu navigation
		//navigate = 1;
		/* Record <<
		// Wifi
		// Server
		// Exit
		*/
		__disable_irq();
		ssd1306_Clear();
		if(navigate < PAGE_NAVIGATION && Menu.counting < MENU_NUMBER - 1) {
			navigate++;
			ssd1306_Clear();
		}
		else if (Menu.counting == MENU_NUMBER - 1){
			navigate = 0;
		}
		previous_millis = current_millis;
		ssd1306_Clear();
		__enable_irq();
	}

	if(GPIO_Pin == GPIO_PIN_15 && Debounce){
		//Position Function in Menu navigation
		//navigate = 1;
		/* Record
		// Wifi   <<
		// Server
		// Exit
		*/
		__disable_irq();
		ssd1306_Clear();
		if(navigate >= PAGE_NAVIGATION){
			navigate--;
		}
		menu_button_plus(&Menu);
		previous_millis = current_millis;
		ssd1306_Clear();
	    __enable_irq();
	}

}

//Error with DMA
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	HAL_UART_DeInit(huart);
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartADC */
/**
  * @brief  Function implementing the ADC thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartADC */
void StartADC(void const * argument)
{
  /* USER CODE BEGIN 5 */

  ssd1306_Init();
  ssd1306_FlipScreenVertically();
  ssd1306_Clear();
  ssd1306_SetColor(White);

  osDelay(500);

  //char data[8]; //buffer for Serial UART1
  char esp_char_buffer[5]; //buffer for esp_buffer
  uint16_t k = 0;
  osDelay(500);
  HAL_UART_Transmit_DMA(&huart1, (uint8_t *) "SL", 3);
  uint8_t n = 0;
  uint8_t count_sample = 0; //Down sampling by 5 (500/5 = 100 sample/sec)
  uint16_t count_buffer = 0; //Count buffer for plotting
  uint16_t esp_adc;

  //Uart rx buffer
  char rx_esp[11] = {0};
  /* Infinite loop */
  for(;;)
  {
	//uint32_t now, count;
	//now = HAL_GetTick();
	if(oled_flag & SET_FLAG){
		oled_flag &= ~SET_FLAG;
		sleep_count = 0;
		//oled_flag &= CLEAR_FLAG;
		menu_pointing[navigate](&Menu);
		if(navigate == PAGE_NAVIGATION && Menu.counting == 2){
			//Reset Esp8266
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
			osDelay(100);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);
			osDelay(500);
			HAL_UART_Transmit_DMA(&huart1, (uint8_t *) "WF", 4);
			//osDelay(1000);
			//HAL_UART_Transmit_DMA(&huart1, (uint8_t *) "SL", 4);
		}
	}

	if(navigate == PAGE_NAVIGATION && (Menu.counting == 0 || Menu.counting == 1) ){
	  //HAL_SPI_MspDeInit(&hspi1);

	  global_flag |= OPENSD_FLAG;
	  osDelay(1000);
	  //Clear SD buffer
	  k = 0; n = 0;
	  if(Menu.counting == 0){

		  //Reset Esp8266
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
		  osDelay(100);
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);
		  osDelay(500);
		  HAL_UART_Transmit_DMA(&huart1, (uint8_t *) "RT", 3);

		  //Clear Plot buffer
		  count_buffer = 0; count_sample = 0;
		  esp_flag &= CLEAR_FLAG;
		  memset(&esp_send_buffer_1, 0, sizeof(esp_send_buffer_1));
		  memset(&esp_send_buffer_2, 0, sizeof(esp_send_buffer_2));
		  //osDelay(5000);
		  //UART Buffer
		  __disable_irq();
		  memset(&rx_esp,0, sizeof(rx_esp));
		  HAL_UART_Receive(&huart1,(uint8_t *) rx_esp, 10, HAL_MAX_DELAY);

		  //Wifi connection
		  ssd1306_SetCursor (0,20);
		  ssd1306_WriteString (rx_esp+1, Font_11x18);
		  ssd1306_UpdateScreen();
		  __enable_irq();
		  osDelay(2000);
	  }

	  while(navigate== PAGE_NAVIGATION && (Menu.counting == 0 || Menu.counting == 1)){

	  		//ADC sampling rate fs = 500Hz
	  		HAL_ADC_Start_DMA(&hadc1,  (uint32_t *) &adc_buffer, 1);
	  		//shift x[n] = [0, 0, 0, 0, 10] -> x[n] = [0, 0, 0, 10, 5]
	  		delay_signal_x(ecg_data.x, N_X);
	  		ecg_data.x[N_X-1] = (uint16_t) (adc_buffer);

	  		//Bandpass filter
	  		delay_signal_x(ecg_data.y_pass, N_PASS);
	  		ecg_data.y_pass[N_PASS-1] = bandpass_filter_x(ecg_data.x, ecg_data.y_pass, N_PASS);

	  		//Notch filter
	  		delay_signal_x(ecg_data.y_notch_1, N_NOTCH);
	  		ecg_data.y_notch_1[N_NOTCH-1] = notch_filter_1_x(ecg_data.y_pass, ecg_data.y_notch_1, N_NOTCH);

	  		//
	  		//Display ECG via UART1
	  		//memset(&data, 0, sizeof(data));
	  		//sprintf(data, "%d\r\n", ecg_data.y_notch_1[N_NOTCH-1]);
	  		//HAL_UART_Transmit_DMA(&huart1, (uint8_t *) data, sizeof(data));

	  		n++;
	  		//For storing data
	  		//Down sampling rate fs = 250Hz
	  		if(n==2){
	  			n=0;
	  			//Append buffer
	  			if(k < N_SEND){
	  			  global_adc[k] = ecg_data.y_notch_1[N_NOTCH-1];
	  			}
	  			//Set Write data Event flag
	  			else if (k>= N_SEND){
	  			  k = 0;
	  			  global_flag |= SEND_FLAG;
	  			  osMutexRelease(MutexHandle);
	  			}
	  			k++;
	  		}
	  		count_sample++;
	  		//For plotting data
	  		//Downsampling rate fs = 100Hz
	  		if(count_sample == 5 && Menu.counting == 0){
	  			count_sample = 0;
	  			//Send via UART_3 if buffer = 100 samples
	  			//Send data per second
	  			if(count_buffer >= N_BUFFER_ESP){
	  				count_buffer = 0;
	  				//Toggle buffer
	  				esp_flag ^= BUFFER_1_FLAG;
	  				if(esp_flag & BUFFER_1_FLAG){
	  					memset(&esp_send_buffer_2, 0, sizeof(esp_send_buffer_2));
	  					HAL_UART_Transmit_DMA(&huart1, (uint8_t *) esp_send_buffer_1, sizeof(esp_send_buffer_1));

	  				}
	  				else{
	  					memset(&esp_send_buffer_1, 0, sizeof(esp_send_buffer_1));
	  					HAL_UART_Transmit_DMA(&huart1, (uint8_t *) esp_send_buffer_2, sizeof(esp_send_buffer_2));
	  				}
	  			}
	  			//Store data to buffer
	  			else if (count_buffer < N_BUFFER_ESP){
	  				esp_adc = ecg_data.y_notch_1[N_NOTCH-1]/20;
	  				if(esp_adc >= 100){
	  					esp_adc = 0;
	  				}
	  				memset(&esp_char_buffer, 0, sizeof(esp_char_buffer));
	  				sprintf(esp_char_buffer, "%-2d\n", esp_adc);
	  				if(~(esp_flag & BUFFER_1_FLAG)){
	  					strcat(esp_send_buffer_1, esp_char_buffer);
	  				}
	  				else{
	  					strcat(esp_send_buffer_2, esp_char_buffer);
	  				}
	  				count_buffer++;
	  			}
	  		}
	  		osDelay(2);
	  	  }
	  	  //HAL_UART_DMAStop(&huart1);
	  	  osDelay(500);
	  	  HAL_UART_Transmit_DMA(&huart1, (uint8_t *) "END", 4);

	  	  //Set Save file Event flag

	  	  global_flag |= SAVESD_FLAG;
	}
	else{
		sleep_count++;
	}

	//Sleep
	if((navigate == PAGE_NAVIGATION && Menu.counting == 3) || sleep_count >= 20000){
		sleep_count = 0;
		global_flag &= CLEAR_FLAG;
		ssd1306_Clear();
		ssd1306_UpdateScreen();
		HAL_UART_Transmit_DMA(&huart1, (uint8_t *) "SL", 3);
		navigate = 0;
		Menu.counting = 0; Menu.pointing = 0; Menu.position = 0;
		HAL_SuspendTick();
		HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
		//HAL_ResumeTick();
	}

	osDelay(1);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartSD */
/**
* @brief Function implementing the SD thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartSD */
void StartSD(void const * argument)
{
  /* USER CODE BEGIN StartSD */
	//HAL_SPI_MspDeInit(&hspi1);
	char sd_buffer[5];
	char num_to_char[4];
	Mount_SD("/");
	osDelay(500);
	HAL_SPI_MspDeInit(&hspi1);
  /* Infinite loop */
  for(;;)
  {
	//Create file Event
	if(global_flag&OPENSD_FLAG){
	  osDelay(500);
      HAL_SPI_MspInit(&hspi1);

      global_flag &= ~OPENSD_FLAG;
      //global_flag &= CLEAR_FLAG;
	  Open_File("");
	}

	//Write data Event
	if(global_flag&SEND_FLAG){
		//Write data to SD_Card File
		osMutexWait(MutexHandle, osWaitForever);
		global_flag &= ~SEND_FLAG;
		//global_flag &= CLEAR_FLAG;
		for(int i = 1; i < N_SEND; i++){
			num_to_char[0] = global_adc[i]/1000;
			num_to_char[1] = (global_adc[i]/100)%10;
			num_to_char[2] = (global_adc[i]/10)%10;
			num_to_char[3] = global_adc[i]%10;

			sd_buffer[0] = num_to_char[0] + 48;
			sd_buffer[1] = num_to_char[1] + 48;
			sd_buffer[2] = num_to_char[2] + 48;
			sd_buffer[3] = num_to_char[3] + 48;
			sd_buffer[4] = '\n';
			Write_File(sd_buffer);
		}
		osMutexRelease(MutexHandle);
	}

	//Save file Event
	if(global_flag & SAVESD_FLAG){
		global_flag &= ~SAVESD_FLAG;
		//global_flag &= CLEAR_FLAG;
		Close_File();
		osDelay(100);
		HAL_SPI_MspDeInit(&hspi1);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
		osDelay(1000);
		HAL_UART_Transmit_DMA(&huart1, (uint8_t *) "SD", 3);
	}
    osDelay(1);
  }
  /* USER CODE END StartSD */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
