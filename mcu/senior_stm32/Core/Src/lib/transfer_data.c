/*
 * File_Handling_RTOS.c
 *
 *  Created on: 30-April-2020
 *      Author: Controllerstech
 */

#include "lib/transfer_data.h"
#include "stm32f1xx_hal.h"
#include "stdlib.h"
//extern UART_HandleTypeDef huart1;
//#define UART &huart1



/* =============================>>>>>>>> NO CHANGES AFTER THIS LINE =====================================>>>>>>> */

FATFS fs;  // file system
FIL fil; // File
FILINFO fno;
FRESULT fresult;  // result
UINT br, bw;  // File read/write count

/**** capacity related *****/
FATFS *pfs;
DWORD fre_clust;
uint32_t total, free_space;



void Mount_SD (const TCHAR* path)
{
	fresult = f_mount(&fs, path, 1);
}

void Unmount_SD (const TCHAR* path)
{
	fresult = f_mount(NULL, path, 0);

}


FRESULT Close_File (){
	fresult = f_close(&fil);
	return fresult;
}

FRESULT Open_File (char *name){
	if(!strcmp(name, "")){
		int i = 0;
		char create_file[10];
		char update[] = "file.txt";
		char write_update[6];
		while(1){
			memset(&create_file, 0, sizeof(create_file));
			sprintf(create_file, "%04d.txt", i);
			fresult = f_open(&fil, create_file, FA_CREATE_NEW|FA_READ|FA_WRITE);
			if (fresult == FR_EXIST)
			{
				i++;
			}
			else if (fresult == FR_OK){
				sprintf(write_update, "%04d", i);
				f_close(&fil);
				break;
			}
		}
		/* update index */
		f_open(&fil, update, FA_OPEN_ALWAYS|FA_READ|FA_WRITE);
		fresult = f_puts((const TCHAR*)write_update, &fil);
		f_close(&fil);
		/*open new created file */
		fresult = f_open(&fil, create_file, FA_OPEN_ALWAYS|FA_READ|FA_WRITE);
		return fresult;
	}
	else{
		fresult = f_open(&fil, name, FA_OPEN_ALWAYS|FA_READ|FA_WRITE);
		return fresult;
	}
}

FRESULT Write_File (char *data){
	    /* Create a file with read write access and open it */
	    //fresult = f_write(&fil, data, strlen(data), &bw);
		//fresult = f_printf(&fil, (const TCHAR *) data);
		fresult = f_puts((const TCHAR*)data, &fil);
		//f_sync(&fil);
		return fresult;
}

FRESULT Read_File (char *name)
{
	/**** check whether the file exists or not ****/
	fresult = f_stat (name, &fno);
	if (fresult != FR_OK)
	{
		char *buf = pvPortMalloc(100*sizeof(char));
		sprintf (buf, "ERRROR!!! *%s* does not exists\n\n", name);
		Send_Uart (buf);
		vPortFree(buf);
	    return fresult;
	}

	else
	{
		/* Open file to read */
		fresult = f_open(&fil, name, FA_READ);

		if (fresult != FR_OK)
		{
			char *buf = pvPortMalloc(100*sizeof(char));
			sprintf (buf, "ERROR!!! No. %d in opening file *%s*\n\n", fresult, name);
		    Send_Uart(buf);
		    vPortFree(buf);
		    return fresult;
		}

		/* Read data from the file
		* see the function details for the arguments */

		char *buffer = pvPortMalloc(sizeof(f_size(&fil)));
		fresult = f_read (&fil, buffer, f_size(&fil), &br);
		if (fresult != FR_OK)
		{
			char *buf = pvPortMalloc(100*sizeof(char));
			vPortFree(buffer);
		 	sprintf (buf, "ERROR!!! No. %d in reading file *%s*\n\n", fresult, name);
		  	Send_Uart(buffer);
		  	vPortFree(buf);
		}

		else
		{
			Send_Uart(buffer);
			vPortFree(buffer);

			/* Close file */
			fresult = f_close(&fil);
			if (fresult != FR_OK)
			{
				char *buf = pvPortMalloc(100*sizeof(char));
				sprintf (buf, "ERROR!!! No. %d in closing file *%s*\n\n", fresult, name);
				Send_Uart(buf);
				vPortFree(buf);
			}
			else
			{
				char *buf = pvPortMalloc(100*sizeof(char));
				sprintf (buf, "File *%s* CLOSED successfully\n", name);
				Send_Uart(buf);
				vPortFree(buf);
			}
		}
	    return fresult;
	}
}



FRESULT Remove_File (char *name)
{
	/**** check whether the file exists or not ****/
	fresult = f_stat (name, &fno);
	if (fresult != FR_OK)
	{

	}

	else
	{
		fresult = f_unlink (name);
		if (fresult == FR_OK)
		{

		}

		else
		{

		}
	}
	return fresult;
}

FRESULT Create_Dir (char *name)
{
    fresult = f_mkdir(name);
    if (fresult == FR_OK)
    {

    }
    else
    {

    }
    return fresult;
}

void Check_SD_Space (void)
{
    /* Check free space */
    f_getfree("", &fre_clust, &pfs);

    total = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
    char *buf = pvPortMalloc(30*sizeof(char));
    sprintf (buf, "SD CARD Total Size: \t%lu\n",total);
    Send_Uart(buf);
    vPortFree(buf);
    free_space = (uint32_t)(fre_clust * pfs->csize * 0.5);
    buf = pvPortMalloc(30*sizeof(char));
    sprintf (buf, "SD CARD Free Space: \t%lu\n",free_space);
    Send_Uart(buf);
    vPortFree(buf);
}
