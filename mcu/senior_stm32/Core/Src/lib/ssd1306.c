/*
 * ssd1306.c
 *
 *  Created on: 14/04/2018
 *  Update on: 10/04/2019
 *      Author: Andriy Honcharenko
 *      version: 2
 *
 *  Modify on: 17/08/2022
 *      Author: Roberto Benjami
 *  Added features in DMA mode:
 *  - ssd1306_UpdateScreen works without blocking
 *  - you can query that UpdateScreen is complete (if ssd1306_UpdateScreenCompleted() == 1)
 *  - callback function if UpdateScreen is complete (ssd1306_UpdateCompletedCallback)
 *  Added features in DMA mode with continuous display update:
 *  - enable continuous display update
 *  - disable continuous display update
 *  - enable raster interrupt(s) of PAGEx (you can set which PAGE(s) )
 */

#include <math.h>
#include "lib/ssd1306.h"

#if SSD1306_USE_DMA == 0 && SSD1306_CONTUPDATE == 1
#error SSD1306_CONTUPDATE only in DMA MODE !
#endif

// Screen object
static SSD1306_t SSD1306;
// Screenbuffer
static uint8_t SSD1306_Buffer[SSD1306_BUFFER_SIZE];
// SSD1306 display geometry
SSD1306_Geometry display_geometry = SSD1306_GEOMETRY;

//
//  Get a width and height screen size
//
static const uint16_t width(void)  { return SSD1306_WIDTH; };
static const uint16_t height(void)  { return SSD1306_HEIGHT; };

uint16_t ssd1306_GetWidth(void)
{
  return SSD1306_WIDTH;
}

uint16_t ssd1306_GetHeight(void)
{
  return SSD1306_HEIGHT;
}

SSD1306_COLOR ssd1306_GetColor(void)
{
  return SSD1306.Color;
}

void ssd1306_SetColor(SSD1306_COLOR color)
{
  SSD1306.Color = color;
}

//  Initialize the oled screen
uint8_t ssd1306_Init(void)
{
  /* Check if LCD connected to I2C */
  if (HAL_I2C_IsDeviceReady(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 5, 1000) != HAL_OK)
  {
    SSD1306.Initialized = 0;
    /* Return false */
    return 0;
  }

  // Wait for the screen to boot
  HAL_Delay(100);

  /* Init LCD */
  ssd1306_WriteCommand(DISPLAYOFF);
  ssd1306_WriteCommand(SETDISPLAYCLOCKDIV);
  ssd1306_WriteCommand(0xF0); // Increase speed of the display max ~96Hz
  ssd1306_WriteCommand(SETMULTIPLEX);
  ssd1306_WriteCommand(height() - 1);
  ssd1306_WriteCommand(SETDISPLAYOFFSET);
  ssd1306_WriteCommand(0x00);
  ssd1306_WriteCommand(SETSTARTLINE);
  ssd1306_WriteCommand(CHARGEPUMP);
  ssd1306_WriteCommand(0x14);
  ssd1306_WriteCommand(MEMORYMODE);
  ssd1306_WriteCommand(0x00);
  ssd1306_WriteCommand(SEGREMAP);
  ssd1306_WriteCommand(COMSCANINC);
  ssd1306_WriteCommand(SETCOMPINS);

  if (display_geometry == GEOMETRY_128_64)
  {
    ssd1306_WriteCommand(0x12);
  }
  else if (display_geometry == GEOMETRY_128_32)
  {
    ssd1306_WriteCommand(0x02);
  }

  ssd1306_WriteCommand(SETCONTRAST);

  if (display_geometry == GEOMETRY_128_64)
  {
    ssd1306_WriteCommand(0xCF);
  }
  else if (display_geometry == GEOMETRY_128_32)
  {
    ssd1306_WriteCommand(0x8F);
  }

  ssd1306_WriteCommand(SETPRECHARGE);
  ssd1306_WriteCommand(0xF1);
  ssd1306_WriteCommand(SETVCOMDETECT); //0xDB, (additionally needed to lower the contrast)
  ssd1306_WriteCommand(0x40);          //0x40 default, to lower the contrast, put 0
  ssd1306_WriteCommand(DISPLAYALLON_RESUME);
  ssd1306_WriteCommand(NORMALDISPLAY);
  ssd1306_WriteCommand(0x2e);            // stop scroll
  ssd1306_WriteCommand(DISPLAYON);

  // Set default values for screen object
  SSD1306.CurrentX = 0;
  SSD1306.CurrentY = 0;
  SSD1306.Color = Black;

  // Clear screen
  ssd1306_Clear();

  // Continuous Update on
  ssd1306_ContUpdateEnable();

  // Flush buffer to screen
  ssd1306_UpdateScreen();

  SSD1306.Initialized = 1;


  /* Return OK */
  return 1;
}

//
//  Fill the whole screen with the given color
//
void ssd1306_Fill(void)
{
  /* Set memory */
  uint32_t i;

  for(i = 0; i < sizeof(SSD1306_Buffer); i++)
  {
    SSD1306_Buffer[i] = (SSD1306.Color == Black) ? 0x00 : 0xFF;
  }
}

//
//  Draw one pixel in the screenbuffer
//  X => X Coordinate
//  Y => Y Coordinate
//  color => Pixel color
//
void ssd1306_DrawPixel(uint8_t x, uint8_t y)
{
  SSD1306_COLOR color = SSD1306.Color;

  if (x >= ssd1306_GetWidth() || y >= ssd1306_GetHeight())
  {
    // Don't write outside the buffer
    return;
  }

  // Check if pixel should be inverted
  if (SSD1306.Inverted)
  {
    color = (SSD1306_COLOR) !color;
  }

  // Draw in the right color
  if (color == White)
  {
    SSD1306_Buffer[x + (y / 8) * width()] |= 1 << (y % 8);
  }
  else
  {
    SSD1306_Buffer[x + (y / 8) * width()] &= ~(1 << (y % 8));
  }
}

void ssd1306_DrawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1)
{
  int16_t steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep)
  {
    SWAP_INT16_T(x0, y0);
    SWAP_INT16_T(x1, y1);
  }

  if (x0 > x1)
  {
    SWAP_INT16_T(x0, x1);
    SWAP_INT16_T(y0, y1);
  }

  int16_t dx, dy;
  dx = x1 - x0;
  dy = abs(y1 - y0);

  int16_t err = dx / 2;
  int16_t ystep;

  if (y0 < y1)
  {
    ystep = 1;
  }
  else
  {
    ystep = -1;
  }

  for (; x0<=x1; x0++)
  {
    if (steep)
    {
      ssd1306_DrawPixel(y0, x0);
    }
    else
    {
      ssd1306_DrawPixel(x0, y0);
    }
    err -= dy;
    if (err < 0)
    {
      y0 += ystep;
      err += dx;
    }
  }
}


char ssd1306_WriteChar(char ch, FontDef Font)
{
  uint32_t i, b, j;

  // Check remaining space on current line
  if (width() <= (SSD1306.CurrentX + Font.FontWidth) ||
    height() <= (SSD1306.CurrentY + Font.FontHeight))
  {
    // Not enough space on current line
    return 0;
  }

  // Use the font to write
  for (i = 0; i < Font.FontHeight; i++)
  {
    b = Font.data[(ch - 32) * Font.FontHeight + i];
    for (j = 0; j < Font.FontWidth; j++)
    {
      if ((b << j) & 0x8000)
      {
        ssd1306_DrawPixel(SSD1306.CurrentX + j, SSD1306.CurrentY + i);
      }
      else
      {
        SSD1306.Color = !SSD1306.Color;
        ssd1306_DrawPixel(SSD1306.CurrentX + j, SSD1306.CurrentY + i);
        SSD1306.Color = !SSD1306.Color;
      }
    }
  }

  // The current space is now taken
  SSD1306.CurrentX += Font.FontWidth;

  // Return written char for validation
  return ch;
}

//
//  Write full string to screenbuffer
//
char ssd1306_WriteString(char* str, FontDef Font)
{
  // Write until null-byte
  while (*str)
  {
    if (ssd1306_WriteChar(*str, Font) != *str)
    {
      // Char could not be written
      return *str;
    }

    // Next char
    str++;
  }

  // Everything ok
  return *str;
}

//
//  Position the cursor
//
void ssd1306_SetCursor(uint8_t x, uint8_t y)
{
  SSD1306.CurrentX = x;
  SSD1306.CurrentY = y;
}

void ssd1306_Clear()
{
  memset(SSD1306_Buffer, 0, SSD1306_BUFFER_SIZE);
}

#if SSD1306_USE_DMA == 0

//
//  Send a byte to the command register
//
void ssd1306_WriteCommand(uint8_t command)
{
  HAL_I2C_Mem_Write(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &command, 1, 10);
}

void ssd1306_WriteData(uint8_t* data, uint16_t size)
{
  HAL_I2C_Mem_Write(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x40, 1, data, size, 100);
}

//
//  Write the screenbuffer with changed to the screen
//
void ssd1306_UpdateScreen(void)
{
  uint8_t i;
  for (i = 0; i < SSD1306_HEIGHT / 8; i++)
  {
    ssd1306_WriteCommand(0xB0 + i);
    ssd1306_WriteCommand(SETLOWCOLUMN);
    ssd1306_WriteCommand(SETHIGHCOLUMN);
    ssd1306_WriteData(&SSD1306_Buffer[SSD1306_WIDTH * i], width());
  }
}

#elif SSD1306_USE_DMA == 1

volatile uint8_t ssd1306_updatestatus = 0;
volatile uint8_t ssd1306_updateend;
uint8_t i2c_command = 0;

#if SSD1306_CONTUPDATE == 0

//
//  Send a byte to the command register
//
void ssd1306_WriteCommand(uint8_t command)
{
  while(ssd1306_updatestatus);
  while(HAL_I2C_GetState(&SSD1306_I2C_PORT) != HAL_I2C_STATE_READY) { };
  i2c_command = command;
  HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &i2c_command, 1);
}

//
//  Write the screenbuffer with changed to the screen
//
void ssd1306_UpdateScreen(void)
{
  if(ssd1306_updatestatus == 0)
  {
    ssd1306_updatestatus = SSD1306_HEIGHT;
    ssd1306_updateend = SSD1306_HEIGHT + (SSD1306_HEIGHT / 2);
    i2c_command = 0xB0;
    HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &i2c_command, 1);
  }
  else if(ssd1306_updatestatus >= SSD1306_HEIGHT)
  {
    ssd1306_updatestatus -= (SSD1306_HEIGHT / 2);
    ssd1306_updateend = (ssd1306_updatestatus + (SSD1306_HEIGHT / 2 + 1)) & 0xFC;
  }
}

char ssd1306_UpdateScreenCompleted(void)
{
  if(ssd1306_updatestatus)
    return 0;
  else
    return 1;
}

__weak void ssd1306_UpdateCompletedCallback(void) { };

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  uint32_t phase;
  if(hi2c->Instance == SSD1306_I2C_PORT.Instance)
  {
    if(ssd1306_updatestatus)
    {
      if(ssd1306_updatestatus < ssd1306_updateend)
      {
        ssd1306_updatestatus++;
        phase = ssd1306_updatestatus & 3;
        if(phase == 3)
          HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x40, 1, &SSD1306_Buffer[SSD1306_WIDTH * ((ssd1306_updatestatus >> 2) & (SSD1306_HEIGHT / 8 - 1))], width());
        else
        {
          if(phase == 0)
            i2c_command = 0xB0 + ((ssd1306_updatestatus >> 2) & (SSD1306_HEIGHT / 8 - 1));
          else if(phase == 1)
            i2c_command = SETLOWCOLUMN;
          else if(phase == 2)
            i2c_command = SETHIGHCOLUMN;
          HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &i2c_command, 1);
        }
      }
      else
      {
        ssd1306_updatestatus = 0;
        ssd1306_UpdateCompletedCallback();
      }
    }
  }
}

#elif SSD1306_CONTUPDATE == 1

volatile uint8_t ssd1306_command = 0;
volatile uint8_t ssd1306_ContUpdate = 0;
volatile uint8_t ssd1306_RasterIntRegs = 0;

//
//  Send a byte to the command register
//
void ssd1306_WriteCommand(uint8_t command)
{
  if(ssd1306_updatestatus)
  {
    while(ssd1306_command);
    ssd1306_command = command;
  }
  else
  {
    while(HAL_I2C_GetState(&SSD1306_I2C_PORT) != HAL_I2C_STATE_READY) { };
    i2c_command = command;
    HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &i2c_command, 1);
  }
}

void ssd1306_ContUpdateEnable(void)
{
  if(!ssd1306_ContUpdate)
  {
    while(HAL_I2C_GetState(&SSD1306_I2C_PORT) != HAL_I2C_STATE_READY) { };
    ssd1306_updatestatus = SSD1306_HEIGHT;
    ssd1306_updateend = SSD1306_HEIGHT + (SSD1306_HEIGHT / 2);
    ssd1306_ContUpdate = 1;
    i2c_command = 0xB0;
    HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &i2c_command, 1);
  }
}

void ssd1306_ContUpdateDisable(void)
{
  if(ssd1306_ContUpdate)
  {
    ssd1306_ContUpdate = 0;
    while(ssd1306_updatestatus) { };
  }
}

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  uint32_t phase;
  uint8_t  raster;
  if(hi2c->Instance == SSD1306_I2C_PORT.Instance)
  {
    if(ssd1306_updatestatus)
    {
      if(ssd1306_updatestatus < ssd1306_updateend)
      {
        ssd1306_updatestatus++;
        phase = ssd1306_updatestatus & 3;
        if(phase == 3)
        {
          raster = (ssd1306_updatestatus >> 2) & (SSD1306_HEIGHT / 8 - 1);
          if(ssd1306_RasterIntRegs & (1 << raster))
            ssd1306_RasterIntCallback(raster);
          HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x40, 1, &SSD1306_Buffer[SSD1306_WIDTH * ((ssd1306_updatestatus >> 2) & (SSD1306_HEIGHT / 8 - 1))], width());
        }
        else
        {
          if(phase == 0)
            i2c_command = 0xB0 + ((ssd1306_updatestatus >> 2) & (SSD1306_HEIGHT / 8 - 1));
          else if(phase == 1)
            i2c_command = SETLOWCOLUMN;
          else if(phase == 2)
            i2c_command = SETHIGHCOLUMN;
          HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &i2c_command, 1);
        }
      }
      else
      { /* refresh end */
        if(ssd1306_command)
        { /* command ? */
          i2c_command = ssd1306_command;
          ssd1306_command = 0;
        }
        else
        { /* refresh restart */
          if(ssd1306_ContUpdate)
          {
            i2c_command = 0xB0;
            ssd1306_updatestatus = SSD1306_HEIGHT;
          }
          else
          {
            ssd1306_updatestatus = 0;
          }
        }

        if(ssd1306_updatestatus)
          HAL_I2C_Mem_Write_DMA(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &i2c_command, 1);
      }
    }
  }
}

void ssd1306_SetRasterInt(uint8_t r)
{
  ssd1306_RasterIntRegs = r;
}

__weak void ssd1306_RasterIntCallback(uint8_t r)
{

}

#endif

#endif
