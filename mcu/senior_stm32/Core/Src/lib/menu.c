/**
 * original author:  Tilen Majerle<tilen@majerle.eu>
 * modification for STM32f10x: Alexander Lutsai<s.lyra@ya.ru>
   ----------------------------------------------------------------------
   MENU
   ----------------------------------------------------------------------
 */
#include "main.h"
#include "lib/menu.h"
#include "lib/fonts.h"
#include "lib/ssd1306.h"
#include "lib/dsp.h"
#include "stdio.h"
#include "cmsis_os.h"
/* External variable */


/* Define variable */
struct MenuPosition Menu = {.position = 0, .pointing = 0, .counting = 0};

const char pointing_arr[] = {0, 20, 40};


char *chose[] = {"ECG Onl",
				 "ECG Off",
				 "Wifi",
				 "Sleep"};

char *menu[] = {"ECG Onl",
				"ECG Off",
				"Wifi",
				"Sleep",
				"Exit"};

/* Define Function */
void menu_start(struct MenuPosition *Menu){
	osDelay(10);
	Menu->position = 0; Menu->pointing = 0; Menu->counting = 0;
	ssd1306_SetCursor (20,20);
	ssd1306_WriteString ("--START--", Font_11x18);
	ssd1306_UpdateScreen();
}

void menu_choice(struct MenuPosition *Menu){
	osDelay(10);
	ssd1306_SetCursor (0,0);
	ssd1306_WriteString (chose[Menu->counting], Font_11x18);
	//ssd1306_SetCursor (80,40);
	//ssd1306_WriteString ("Exit", Font_11x18);
	ssd1306_UpdateScreen();
}

void menu_display(struct MenuPosition *Menu){
	osDelay(1);
	ssd1306_SetCursor (0,0);
	ssd1306_WriteString (menu[Menu->position], Font_11x18);

	ssd1306_SetCursor (0,20);
	ssd1306_WriteString (menu[Menu->position+1], Font_11x18);

	ssd1306_SetCursor (0,40);
	ssd1306_WriteString (menu[Menu->position+2], Font_11x18);

	ssd1306_UpdateScreen();

	ssd1306_SetCursor (100, pointing_arr[Menu->pointing]);
	ssd1306_WriteString ("<<", Font_11x18);
	ssd1306_UpdateScreen();

}

void menu_button_plus(struct MenuPosition *Menu){
	ssd1306_Clear();
	(Menu->pointing)++;
	(Menu->counting)++;

	if(Menu->counting >= MENU_NUMBER){ //6
		Menu->counting = 0;
		}
	if(Menu->pointing >= 3){ //
		Menu->pointing = 2;
		Menu->position += 1;
		}

	if(Menu->position >= MENU_NUMBER-2){ //4
		Menu->position = 0;
		Menu->pointing = 0;
		}
}


