

/* Define variable */
#include "lib/dsp.h"
#include "stdio.h"
#include "stdlib.h"
#include "main.h"
/*Variable*/
uint16_t adc_buffer = 0;

//struct ecg_dsp ecg_data;
extern UART_HandleTypeDef huart1;
/*Define Function */
/*---------------*/

//Average filter h[n] = [1/5, 1/5, 1/5, 1/5, 1/5]
//Return y_avg[]
uint16_t average_filter_x(uint16_t x[], uint8_t order){
	uint16_t y;

	for(int j = 0; j < order; j++){
		y += x[j];
	}
	y = y/order;
	return y;
}

//Bandpass filter BW: 1 - 100Hz, fs = 400Hz
/*Define filter */
/*            1  - 2*z^-2 + z^-4
 * H(z) =  G ---------------------------------------------------------
 *            1 - 1.942*z^-1 + 1.395*z^-2 - 0.4017*z^-3 + 0.2053*z^-4
 */
// y[n] = 0.3003*(x[n] - 2*x[n-2] + x[n-4])
// + 1.942*y[n-1] - 1.395*y[n-2] + 0.4017*y[n-3] - 0.2053*y[n-4]
//
uint16_t bandpass_filter_x(uint16_t x[], uint16_t y[], uint8_t order){
	uint8_t n = order - 1;
	uint16_t output = 0;
	//y[n] = (uint16_t) pass_b[1]*y[n-1] + pass_b[2]*y[n-2] + pass_b[3]*y[n-3] + pass_b[4]*y[n-4] + gain_pass_a*(pass_a[0]*x[n] - pass_a[2]*x[n-2] + pass_a[4]*x[n-4]);
	for (int i = 0; i < order; i++){
	  output += pass_b[i]*y[n-i] + gain_pass_a*pass_a[i]*x[n-i];
	}
	return output;
}

//Notch filter
//x[5] = {x[n], x[n-1], x[n-2], x[n-3], x[n-4]}
//y[3] = {y[n], y[n-1],y[n-2]}
uint16_t notch_filter_1_x(uint16_t x[], uint16_t y[], uint8_t order){
	uint8_t n = order - 1;
	uint16_t output = 0;
	for (int i = 0; i < order; i ++){
		output += notch_b_1[i]*y[n-i] + notch_a_1[i]*x[n-i];
	}
	return output;
}

/*uint16_t notch_filter_3_x(uint16_t x[], uint16_t y[], uint8_t order){
	uint8_t n = order - 1;
	uint16_t output = 0;
	for (int i = 0; i < order; i ++){
		output += notch_b_3[i]*y[n-i] + notch_a_3[i]*x[n-i];
	}
	return output;
}
*/

// x[5] = {0, 0, 0, 0, 10} -> x[5] = {0, 0, 0, 10, 5}; -> x[5] = {0, 0, 10, 5, 0}
void delay_signal_x(uint16_t x[], uint8_t n_x){
	uint8_t n = n_x-1;
	for(int i = 0; i < n; i++){
		x[i] = x[i+1];
	}
}


//Clear data of ecg

