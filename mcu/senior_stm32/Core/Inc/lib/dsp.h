/* DSP library
 * Input signal
 * - x[i]
 * Output of average filter
 * - y_avg[i]
 * Output of notch filter
 * - y_notch[i]
 * Output of bandpass filter
 * - y_pass[i]
 */

#include "main.h"

#ifndef DSP_H
#define DSP_H 2000

#ifdef __cplusplus
extern C {
#endif

/* Include Libraries */
#include "stm32f1xx_hal.h"
#include "string.h"

/*Define filter */
/* BandPass filter BW: 1Hz - 100Hz
 *
 *             1  - 2*z^-2 + z^-4
 * H(z) =  G ---------------------------------------------------------
 *            1 - 1.942*z^-1 + 1.395*z^-2 - 0.4017*z^-3 + 0.2053*z^-4
 */
#define gain_pass_a 0.3003
#define pass_a (int[]) {1, 0, -2, 0, 1}
#define pass_b (float[]) {0, 1.942, -1.395, 0.4017, -0.2053}

/*
#define gain_pass_a 0.213
#define pass_a (int[]) {1, 0, -2, 0, 1}
#define pass_b (float[]) {0, 2.3431, -1.9526, 0.8473, -0.2383}
*/
/* Notch Filter
 *
 *
 *            0.9845 -1.5930*z^-1 + 0.9845*z^-2
 * H(z) =  G ---------------------------------------------------------
 *            1 - 1.593*z^-1 + 0.9691*z^-2
 *
 */
#define notch_a_1 (float[]) {0.9845, -1.5930, 0.9845}
#define notch_b_1 (float[]) {0, 1.593, -0.9691}


//#define notch_a_3 (float[]) {0.9845, 0.6085,  0.9845}
//#define notch_b_3 (float[]) {0, -0.6085, -0.9691}

/* Notch Filter
*
*
*            1 -1.618*z^-1 + 1*z^-2
* H(z) =  G ---------------------------------------------------------
*            1 - 1.5164*z^-1 + 0.8783*z^-2
*
*/
//#define notch_a_1 (float[]) {1, -1.618, 1}
//#define notch_b_1 (float[]) {0, 1.5164, -0.8783}


/*Define*/
#define N_X 5
#define N_PASS 5
#define N_NOTCH 3
//#define N_SEND 10
/* External variable
* Use for including variable in main.c
*/
extern uint16_t adc_buffer;
//extern struct ecg_dsp ecg_data;
/*	Structure declare
 */
struct ecg_dsp{
	uint16_t x[N_X];
	uint16_t y_pass[N_PASS];
	uint16_t y_notch_1[N_NOTCH];
	//uint16_t y_send[N_SEND];
};
/* Declare Function */
//Average filter h[n] = [1/5, 1/5, 1/5, 1/5, 1/5]
uint16_t average_filter_x(uint16_t x[], uint8_t order);

// y[n] = G*(x[n] - 2*x[n-2] + x[n-4]) + 1.942*y[n-1] - 1.395*y[n-2] + 0.4017*y[n-3] - 0.2053*y[n-4]
uint16_t bandpass_filter_x(uint16_t x[], uint16_t y[], uint8_t order);

uint16_t notch_filter_1_x(uint16_t x[], uint16_t y[], uint8_t order);
/*uint16_t notch_filter_3_x(uint16_t x[], uint16_t y[], uint8_t order);*/

// x[5] = {0, 0, 0, 0, 10} -> x[5] = {0, 0, 0, 10, 5}; -> x[5] = {0, 0, 10, 5, 0}
void delay_signal_x(uint16_t x[], uint8_t n_x);


#ifdef __cplusplus
}
#endif


#endif
