/**
 *
   ----------------------------------------------------------------------
   Menu library
   OLED SSD1306
   ------
   Page 0
   ---
     --START--
   ------
   Page 1
   ---
   Record <<
   Wifi
   Exit
   ------
   Page 2 (function + display)
   ---
   ADC
   ----------------------------------------------------------------------
 */

#ifndef PAGES_H
#define PAGES_H

#ifdef __cplusplus
extern C {
#endif

/* Include Libraries */
#include "stm32f1xx_hal.h"
#include "string.h"

/*Define*/
#define MENU_NUMBER 5
#define PAGE_NAVIGATION 2
/* External variable
 * Use for including variable in main.c
 */
extern struct MenuPosition Menu;
extern char *chose[];
/*	Structure declare
 */

struct MenuPosition{
	volatile uint8_t position;
	volatile uint8_t pointing;
	volatile uint8_t counting;
};



/* Declare Function */

void menu_start(struct MenuPosition *Menu);
void menu_choice(struct MenuPosition *Menu);
void menu_display(struct MenuPosition *Menu);
void menu_button_plus(struct MenuPosition *Menu);

#ifdef __cplusplus
}
#endif


#endif
