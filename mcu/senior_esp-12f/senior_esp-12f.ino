#include <SPI.h>
#include <SD.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
                                                                                                                                                                                                                               
#include <ESPAsyncTCP.h>
#include "FS.h"
#include <ESPAsyncWebServer.h>
#include "LittleFS.h"

extern "C" {
#include "user_interface.h"
#include "Esp.h"
}

#define SOCKET_BUFFER 60
#define CS 5
#define MOSI 13
#define MISO 12
#define CLK 14
//#include "FS.h"
File myFile;

//Parameter for wifi
AsyncWebServer server(80);
const char* input_parameter1 = "ssid";
const char* input_parameter2 = "pass";
const char* input_parameter3 = "server_ip";
//Wifi management    
String content; 
String wifi_array;
char request_buffer[11];

String ssid;
String pass;
String server_ip;
String http_url;

const int port = 12345;
int n_wifi; //Number of wifi

// File paths to save input wifi permanently
const char* SSID_path = "/ssid.txt";
const char* Password_path = "/pass.txt";
const char* Server_path = "/server_ip.txt";

IPAddress localIP;
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 0, 0);

//const char* ssid = "O2_108";
//const char* password = "O2.108wifi";


//wifi and server connection status
bool wifi_connection;
bool server_connection;
bool wifi_manage_on = false;
/*
const int port = 12345;
char *ip = "172.16.1.2";
char *http_url = "http://172.16.1.2:8090/handle_post";
*/
uint8_t set_up = 0;

// Read File from LittleFS
String readFile(fs::FS &fs, const char * path){
  //Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path,"r");
  if(!file || file.isDirectory()){
    //Serial.println("- failed to open file for reading");
    return String();
  }
  String fileContent;
  while(file.available()){
    fileContent = file.readStringUntil('\n');
    break;     
  }
  file.close();
  return fileContent;
}

// Write file to LittleFS
void writeFile(fs::FS &fs, const char * path, const char * message){
  //Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path,"w");
  if(!file){
    //Serial.println("- failed to open file for writing");
    return;
  }
  file.print(message);
  //if(file.print(message)){
    //Serial.println("- file written");
  //} else {
    //Serial.println("- frite failed");
  //}
}

void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  LittleFS.begin();
  //manage_wifi_handler();
  ssid = readFile(LittleFS, SSID_path);
  pass = readFile(LittleFS, Password_path);
  server_ip = readFile(LittleFS, Server_path);
  http_url = "http://" + server_ip + ":8090/handle_post";
  //Serial.println(ssid);
  //Serial.println(pass);
  //Serial.println(server_ip);
  //Serial.println(http_url);
  //LittleFS.end();
  //"http://172.16.1.2:8090/handle_post";
  //Serial.println("Wake Up");
}


void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available() > 0){
    memset(&request_buffer, 0, sizeof(request_buffer));
    Serial.readBytes(request_buffer, 10);
    //Serial.println(request_buffer);
    //Enter RT
    if(request_buffer[0]== 'R' && request_buffer[1] == 'T'){
      wifi_connection = connect_wifi_handler();
      server_connection = send_tcp_handler(wifi_connection);
    }
    //Enter SD
    else if (request_buffer[0]== 'S' && request_buffer[1] == 'D'){
     //Serial.println("Enter SD RX");
      wifi_connection = connect_wifi_handler();
      //Serial.println("Enter SD func");
      send_sd_handler(server_connection);
      WiFi.disconnect();
      WiFi.forceSleepBegin();
      ESP.deepSleep(0, WAKE_RF_DEFAULT);
    }
    //Enter Wifi
    else if (request_buffer[0]== 'W' && request_buffer[1] == 'F'){
      manage_wifi_handler();
      while(1){
        delay(2000);
        if(wifi_manage_on){
          ESP.deepSleep(0, WAKE_RF_DEFAULT);
          //ESP.restart();
        }
      }
      //ESP.deepSleep(0, WAKE_RF_DEFAULT);
    }
    
    //Enter Sleep
    else if (request_buffer[0]== 'S' && request_buffer[1] == 'L'){
      //Serial.println("Enter Sleep");
      //turn_off_SPI();
      
      //WiFi.disconnect();
      WiFi.forceSleepBegin();
      ESP.deepSleep(0, WAKE_RF_DEFAULT);
    }
  }
}

bool connect_wifi_handler(){
  //WiFi.forceSleepWake();
  //delay(1);
  WiFi.mode(WIFI_STA);
  //Serial.println("WIFI\n");
  uint16_t timeout_wifi = 0;
  
  if(WiFi.status() != WL_CONNECTED){
    WiFi.begin(ssid.c_str(), pass.c_str());
    while (WiFi.status() != WL_CONNECTED && timeout_wifi < 200){
      timeout_wifi++;
      if(WiFi.status() == WL_CONNECTED){
        break;
      }
      //Serial.println("Wifi");
      delay(200);
    }
  }

  //Wifi connect notify
  if(WiFi.status() == WL_CONNECTED){
    Serial.print("W:OK ");
  }
  else{
    Serial.print("W:NC ");
    return false;
  }
  //Serial.print("Status: ");
  //Serial.println(WiFi.status());
  return true;
}

bool send_tcp_handler(bool wifi_connection){
    uint8_t connection_status;
    if(!wifi_connection){
      //Serial.print("S:NC");
      return false;
    }
    WiFiClient client;
    //Connect to server
    //client.connect(ip, port)
    //Serial.print("IP: ");
    //Serial.println(server_ip);
    if(client.connect(server_ip.c_str(), port)){
      Serial.print("S:OK");
    }
    else{
      Serial.print("S:NC");
      return false;
    }
      delay(1000);  
    char rx_buffer[SOCKET_BUFFER+1];
    int i = 0;
    //Serial.setTimeout(100);
    while(1){
      memset(&rx_buffer, 0, sizeof(rx_buffer));
      //delayMicroseconds(1);
      if(Serial.available() > 0){
        Serial.readBytes(rx_buffer, SOCKET_BUFFER);
        //if((rx_buffer[0] != 'E' && rx_buffer[1] != 'N' && rx_buffer[2] != 'D')){
        if(strstr(rx_buffer, "END") == NULL){
          client.print(rx_buffer);
          //Serial.print(rx_buffer);
        }
        else{
          //Serial.println("END");
          client.print("END");
          client.stop();
          break;
        }
      }
    }
    //Serial.println("END TCP");
    delay(500);
    return true;
}

bool send_sd_handler(bool server_connection){
  server_connection = true;
  if(!server_connection){
    return false;
  }
  //Serial.println("Enter SD section");
    SD.begin(CS);
  myFile = SD.open("FILE.TXT", FILE_READ);
  
  char file_name[5];
  String file_send;
  uint8_t k = 0;
  memset(&file_name, 0, sizeof(file_name));
  while (myFile.available()) {
    file_name[k++] = (char) myFile.read();
  }
  memset(&file_send, 0, sizeof(file_send));
  file_send = (String) file_name + ".txt";

  myFile.close();
  myFile = SD.open(file_send);
  //Serial.print("SD open file: ");
  const auto f_size = myFile.size();
  //Serial.println("Open SD");
  //Serial.print("SD: ");
  //Serial.println(file_name);
  //Serial.print("Size: ");
  //Serial.println(f_size);
  HTTPClient http;
  //Serial.println(http_url);
  uint16_t httpCode;
  http.begin(http_url.c_str());
  http.addHeader("Content-Type","application/json");
  http.addHeader("Content-Length", String(f_size, DEC));
  
  //httpCode = http.POST(sendFile);
  httpCode = http.sendRequest("POST", &myFile, myFile.size());
  if (httpCode > 0){
      String payload = http.getString();
      //Serial.println(httpCode);
      //Serial.println(payload);
      //Serial.println("OK HTTP");
      delay(100);
  }
  myFile.close();
  SD.end();
  turn_off_SPI();
  return true;
}

bool manage_wifi_handler(){
  WiFi.mode(WIFI_AP);
  //Serial.println("ESP8266-WIFI-MANAGER");
  WiFi.softAP("ECG-DEVICE-WIFI", NULL);
  IPAddress IP = WiFi.softAPIP();
  //Scan wifi
  
  n_wifi = WiFi.scanNetworks();
    wifi_array = "<ol style=\"margin-left:3em\">";
    for (int i = 0; i < n_wifi; ++i)
    {
      // Print SSID and RSSI for each network found
      wifi_array += "   <li>";
      wifi_array += WiFi.SSID(i);
      wifi_array += "</li>";
    }
    //Serial.println("Scan network");
    wifi_array += "</ol>";
    // Web Server Root URL
       
    content = "<!DOCTYPE HTML>\r\n<html><head><title>ECG Device Manager</title></head>";
    content += "<form action=\"/\" method=\"POST\">";
    content += "<div class=\"content\">";
    content += "<h1>ECG Device Wifi Manager</h1>";
    content += "<h2>Your Wifi Information</h2>";
    content += "<p>";
    content += "<label style=\"font-size:40px;\">SSID: </label><label style=\"font-size:40px;\">" + ssid + "</label><br>";
    content += "<label style=\"font-size:40px;\">PASS: </label><label style=\"font-size:40px;\">*****</label><br>";
    content += "<label style=\"font-size:40px;\">SERVER: </label><label style=\"font-size:40px;\">" + server_ip + "</label><br>";
    content += "</p>";
    content += "<h2>Wifi Connection</h2>";
    content += "<p>";
    content += "<label for=\"ssid\" style=\"font-size:40px;\">SSID </label><input type=\"text\" id=\"ssid\" name=\"ssid\" style=\"font-size:40px;\"><br>";
    content += "<label for=\"pass\" style=\"font-size:40px;\">PASS </label><input type=\"text\" id=\"pass\" name=\"pass\" style=\"font-size:40px;\"><br>";
    content += "<label for=\"server_ip\" style=\"font-size:40px;\">SERVER </label><input type=\"text\" id=\"server_ip\" name=\"server_ip\" style=\"font-size:40px;\"><br>";
    content += "<input type=\"submit\" value=\"OK\" style=\"font-size:40px;\">";
    content += "</p>";
    content += "<h2>Wifi Scan</h2>";
    content += "<p>";
    content += wifi_array;
    content += "</p>";
    content += "</div>";
    content += "</body></html>";
    //Serial.println("HTML");
    //Turn on server
    
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200,"text/html", content);
    });
    server.serveStatic("/", LittleFS, "/");

    server.on("/", HTTP_POST, [](AsyncWebServerRequest *request) {
      int params = request->params();
      for(int i=0;i<params;i++){
        //LittleFS.begin();
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          // HTTP POST ssid value
          if (p->name() == input_parameter1) {
              if(p->value() != ""){
                ssid = p->value().c_str();
                //Serial.print("SSID set to: ");
                //Serial.println(ssid);
                // Write file to save value
                writeFile(LittleFS, SSID_path, ssid.c_str());  
              }
          }
          // HTTP POST pass value
          if (p->name() == input_parameter2) {
              if(p->value() != ""){
                pass = p->value().c_str();
                // Write file to save value
                writeFile(LittleFS, Password_path, pass.c_str());
              }
          }
          // HTTP POST server value
          if (p->name() == input_parameter3) {
            // Write file to save value
            if(p->value() != ""){
              server_ip = p->value().c_str();
              //Serial.print("IP set to: ");
              //Serial.println(server_ip);
              writeFile(LittleFS, Server_path, server_ip.c_str());
            }
          }
        }
      }
      request->send(200, "text/html", "New SSID: " + ssid + "    New Server address: " + server_ip);
      //LittleFS.end();
      wifi_manage_on = true;
    });
    //Serial.println("Server on");
    server.begin();
    //Serial.println("Server end");
    return true;
}
void turn_off_SPI(){
  digitalWrite(CS, LOW);
  digitalWrite(MOSI, LOW);
  digitalWrite(MISO, LOW);
  digitalWrite(CLK, LOW);
}
