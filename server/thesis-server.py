from flask import Flask, render_template, flash, redirect, url_for, session, request, Response, current_app
#from flask_mongoengine import MongoEngine  # ModuleNotFoundError: No module named 'flask_mongoengine' = (venv) C:\flaskmyproject>pip install flask-mongoengine
from werkzeug.utils import secure_filename
import os

from functools import wraps
import pymongo
import codecs
import bson.json_util
import urllib.request
import json
import io
from io import BytesIO
from function_handler import *
from pymongo import MongoClient
from bson.objectid import ObjectId
import matplotlib.pyplot as plt
import datetime
import re
import matplotlib
from sklearn.preprocessing import normalize
#from user import routes
import base64
from user import models

matplotlib.use('SVG')

mongo_client = MongoClient('mongodb+srv://khanh:duykhanh1703@cluster0.axtxkft.mongodb.net/?retryWrites=true&w=majority')
# create new database and collection instance
dbLogin = mongo_client.thesis

app = Flask(__name__, static_url_path='/')
#app.secret_key = "caircocoders-ednalan-2020"
app.secret_key = b'\xcc^\x91\xea\x17-\xd0W\x03\xa7\xf8J0\xac8\xc5'

global_id = ''

def userInfoHandler(userString = "USER: ", adminString = "USER: "):
    userReturn = {}
    try:
        
        userReturn['name'] = "NAME: " + session["user"]["name"]
        userReturn['user'] = userString + session["user"]["user"]
    except:
        userReturn['name'] = "You are in admin mode"
        userReturn['user'] = adminString + 'admin'
    return userReturn

def predictPlotRInterval(data):
    indexLabel = 0.0
    count = [0, 0, 0, 0, 0]
    ##Count beats prediction
    for i in range(24):
        if(data[1250 + 2 + 24 + i] != 5.0):
            count[int(data[1250 + 2 + 24 + i])] += 1
        else:
            break
    if(count[4] > 1):
        indexLabel = 4.0
    elif(count[2] > 1):
        indexLabel = 2.0
    elif(count[3] > 1):
        indexLabel = 3.0
    if(isinstance(indexLabel,float)):
        indexLabel = int(indexLabel)
    labelMatching = {
        0: "Normal beat",
        1: "Supraventricular premature beat",
        2: "Premature ventricular beat",
        3: "Fusion of ventricular and normal beat",
        4: "Unclassifiable beat"
    }
    
    return labelMatching.get(indexLabel, 'ECG Signal')

def beatAnnotation(data):
    mean = data[1250]
    hrv = data[1251]
    
    #Predict ECG Interval based on HRV and mean
    if((mean > 62.5 and mean < 150.0) and (hrv < 12.5)):
        return "Normal Heart Rate"
    elif (mean < 62.5 and (hrv < 12.5)):
        return "Fast Heart Rate"
    elif (mean > 150.0 and (hrv < 12.5)):
        return "Slow Heart Rate"
    elif (hrv > 12.5):
        return "Irregular Heart Rate"
    
    
# Decorators
def login_required(f):
  @wraps(f)
  def wrap(*args, **kwargs):
    if 'logged_in' in session:
      return f(*args, **kwargs)
    else:
      return redirect('/')
  return wrap

@app.route('/404.html')
def error():
    userInfo = {}
    userInfo = userInfoHandler("", "")
    return render_template('404.html', user = userInfo)

@app.route('/')
@app.route('/index_senior.html')
def homepage():
    #
    userInfo = {}
    userInfo = userInfoHandler("", "")
    return render_template('index_senior.html', user = userInfo)

@app.route('/document_list.html')
def document_list():
    userInfo = ""
    userInfo = userInfoHandler("", "")
    urlsRecord = {"user":[], "time":[]}
    urlsPredict = {"user":[], "time":[]}
    
    collectionPredict = dbLogin['ecg']
    if userInfo["user"] == "admin":
        documentRecord_ids = [str(document['_id']) for document in collectionPredict.find({"type": "record"}, {'_id'})]
        documentPredict_ids = [str(document['_id']) for document in collectionPredict.find({"type": "predict"}, {'_id'})]
    else:
        documentRecord_ids = [str(document['_id']) for document in collectionPredict.find({"user": userInfo["user"],"type": "record"}, {'_id'})]
        documentPredict_ids = [str(document['_id']) for document in collectionPredict.find({"user": userInfo["user"], "type": "predict"}, {'_id'})]
    urlsRecord["ids"] = [document_id for document_id in documentRecord_ids]
    for i in range(len(urlsRecord["ids"])):
        urlsRecord['user'] += [str(user_id['user']) for user_id in collectionPredict.find({"_id": ObjectId(urlsRecord["ids"][i])}, {'user'})]
        urlsRecord['time'] += [str(user_id['time']) for user_id in collectionPredict.find({"_id": ObjectId(urlsRecord["ids"][i])}, {'time'})]
    #urlsPredict = {}
    
    urlsPredict["ids"] = [document_id for document_id in documentPredict_ids]
    for i in range(len(urlsPredict["ids"])):
        urlsPredict['user'] += [str(user_id['user']) for user_id in collectionPredict.find({"_id": ObjectId(urlsPredict["ids"][i])}, {'user'})]
        urlsPredict['time'] += [str(user_id['time']) for user_id in collectionPredict.find({"_id": ObjectId(urlsPredict["ids"][i])}, {'time'})]
    print(urlsRecord)
    return render_template('document_list.html', urlsPredict = urlsPredict,  urlsRecord=urlsRecord, user = userInfo)

@app.route('/download/<id>') # use
def download_document(id):
    collection = dbLogin['ecg']
    # Fetch document using id
    document = collection.find_one({'_id': ObjectId(id)})

    try:
        # Convert document to JSON format
        json_data = bson.json_util.dumps(document)
        # Create a file-like object from the string data
        file = io.StringIO(json_data)
        # Create a response object
        response = Response(file.read(), mimetype='application/json')
        response.headers.set('Content-Disposition', 'attachment', filename=f'{id}.json')
        return response
    except:
        # Handle document not found error
        return 'Document not found', 404

    
@app.route('/pipe', methods=["GET", "POST"])
def pipe():
    userInfo = {}
    userInfo = userInfoHandler("","")
    
    global global_id
    collection = dbLogin['ecg']
    document = collection.find_one({'_id': ObjectId(global_id), 'type': 'record'})
    if(document != None):
        try:
            return {"data":document["ecg"]}
        except:
            return render_template("404.html", user = userInfo)
    return ""

@app.route('/document_list/<string:id>')
def document_plot(id):
    global global_id
    # Retrieving all document IDs from MongoDB
    collection = dbLogin['ecg']
    global_id = id
    userInfo = {}
    userInfo = userInfoHandler("","")
    #print(collection.find_one({'_id': ObjectId(id), 'type': 'record'}))
    document = collection.find_one({'_id': ObjectId(id), 'type': 'record'})
    if(document != None):
        document = collection.find_one({'_id': ObjectId(id), 'type': 'record'})
        return render_template('document_visualize.html', document = document, user = userInfo)
    document = collection.find_one({'_id': ObjectId(id), 'type': 'predict'})
    document["_id"] = ObjectId(id)
    document["ecg"] = document["ecg"]
    predict_document = []
    RpeakInterval_document = []
    #print(f"length: {len(document['ecg'])}")
    for i in range(len(document["ecg"])):
        predict_document += [predictPlotRInterval(document['ecg'][i][:])]
        RpeakInterval_document += [beatAnnotation(document['ecg'][i][:])]
        
    return  render_template('document_predict.html', document = document, predict_document = predict_document, RpeakInterval_document = RpeakInterval_document, user = userInfo)

@app.route('/document_beat/<string:id>/<string:beatId>')
def document_beat(id, beatId):
    if beatId == "login.html":
        return redirect("/login.html")
    # Retrieving all document IDs from MongoDB
    collection = dbLogin['ecg']
    userInfo = {}
    userInfo = userInfoHandler("","")
    
    document = collection.find_one({'_id': ObjectId(id), 'type': 'predict'})
    beatIndex = int(beatId)
    RPeakIndex = document['ecg'][beatIndex][1252:1252 + 24]
    RPeakIndex = [int (element) for element in RPeakIndex]
    predictIndex = document['ecg'][beatIndex][1252 + 24 :]
    ##Don't use
    #userInfo["user"] = document["user"]
    
    #print(f"Rpeak = {RPeakIndex}, ({len(RPeakIndex)})")
    #print(f"Rpeak = {predictIndex}, ({len(predictIndex)})")
    maxBeat = len(document["ecg"]) -1
    time_signal = [i/125 for i in range(1250)]
    data = [signal/max(document['ecg'][beatIndex][:-1][:1250]) \
        for signal in document['ecg'][beatIndex][:-1][:1250]]
    #document['ecg'][beatIndex][:-1][:1250] = data
    figure, axis = plt.subplots()
    axis.plot(time_signal, data)
    #Plot R Interval Peak
    for i in range(len(predictIndex)):
            if predictIndex[i] != 5:
                interval = [j/125 for j in range(RPeakIndex[i], RPeakIndex[i] + 120)]
            if predictIndex[i] == 5:
                break
            elif predictIndex[i] == 1:
                axis.plot(interval, data[RPeakIndex[i]:RPeakIndex[i] + 120], color = 'red')
            elif predictIndex[i] == 2:
                axis.plot(interval, data[RPeakIndex[i]:RPeakIndex[i] + 120], color = 'green')
            elif predictIndex[i] == 3:
                axis.plot(interval, data[RPeakIndex[i]:RPeakIndex[i] + 120], color = 'yellow')
            elif predictIndex[i] == 4:
                axis.plot(interval, data[RPeakIndex[i]:RPeakIndex[i] + 120], color = 'black')
    RPeakInterval = beatAnnotation(document['ecg'][beatIndex])
    predictPlotRInterval(document['ecg'][beatIndex])
    axis.set_title(predictPlotRInterval(document['ecg'][beatIndex]), fontsize = 22)
    axis.set_xlabel('Time', fontsize=18)
    axis.set_ylabel('Normalize amplitude', fontsize=18)
    image = BytesIO()
    figure.savefig(image, format='png')
    image.seek(0)
    image_data = base64.b64encode(image.getvalue()).decode()
    plt.clf() 
    return  render_template('document_predict_plot.html', RPeakInterval_document = RPeakInterval, document = document['ecg'][int(beatId)], id = ObjectId(id), beatIndex = beatIndex, maxIndex = maxBeat, user = userInfo, image = image_data, document_user = document["user"])
        
@app.route('/service.html')
def service():
    userInfo = {}
    userInfo = userInfoHandler("", "")
    return render_template('service.html', user = userInfo)

@app.route('/blog-single.html')
def blog():
    userInfo = {}
    userInfo = userInfoHandler("", "")
    return render_template('blog-single.html', user = userInfo)

@app.route('/contact.html')
def contact():
    userInfo = {}
    userInfo = userInfoHandler("", "")
    return render_template('contact.html', user = userInfo)

@app.route('/about.html')
def about():
    userInfo = {}
    userInfo = userInfoHandler("", "")
    return render_template('about.html', user = userInfo)

@app.route('/document_list/login.html')
@app.route('/document_predict/login.html')
def redirectLogin():
    return redirect("/login.html")

@app.route('/login.html')
def loginPage():
    userInfo = {}
    userInfo = userInfoHandler("", "")
    if 'logged_in' in session:
        return render_template('logout.html', user = userInfo)
    else:
        return render_template('login.html', user = userInfo)

@app.route('/register.html')
def register():
    userInfo = {}
    userInfo = userInfoHandler("", "")
    return render_template('register.html', user = userInfo)

@app.route('/handle_post', methods=['POST'])
def handle_post():
    #mongoData: "time" (string), "duration": (int), "ecg": (array of int)
    #Initalize "time" (string) , "ecg" (array of int)
    time_now = datetime.datetime.now()
    #time_now_string = time_now.strftime("%H:%M:%S")
    collectionLogin = dbLogin['login']
    
    #Receive data
    #data = request.get_data().decode("utf-8")
    postedData = request.get_json()
    if(not postedData):
        return "Empty Data"
    postedData['ecg'] = postedData['ecg'][:-1]
    if(postedData['ecg'] == []):
        return "Empty Data ECG"
    
    checkValidUser =  [str(document['_id']) for document in collectionLogin.find({"user": postedData['user']}, {'_id'})]
    if(checkValidUser == []):
        postedData['user'] = "unknown"
    postedData.update({"time": time_now.strftime("%A %d/%m/%Y %H:%M:%S")})
    #print(postedData["time"])   
    #print(f"USER: {postedData['user']}")
    #collection = dbLogin['ecg']
    #result = collection.insert_one(postedData)
    #print(postedData)
    return "Message ok" 
    

@app.route('/user/signup', methods=['POST'])
def signup():
  return models.User().signup()

@app.route('/user/signout')
def signout():
  return models.User().signout()

@app.route('/user/login', methods=['POST'])
def login():
  return models.User().login()

@app.route('/logout.html')
@login_required
def logout():
  userInfo = {}
  userInfo = userInfoHandler("", "")
  return render_template('logout.html', user = userInfo)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port= 8090, debug= True, threaded=True)