from flask import Flask, jsonify, request, session, redirect
from passlib.hash import pbkdf2_sha256
from web_capstone2 import dbLogin
import uuid

class User:

  def start_session(self, user):
    del user['password']
    session['logged_in'] = True
    session['user'] = user
    return jsonify(user), 200

  def signup(self):
    print(request.form)

    # Create the user object
    user = {
      "_id": uuid.uuid4().hex,
      "name": request.form.get('name'),
      "user": request.form.get('user'),
      "password": request.form.get('password')
    }

    # Encrypt the password
    user['password'] = pbkdf2_sha256.encrypt(user['password'])

    # Check for existing email address
    if dbLogin.login.find_one({ "user": user['user'] }):
      return jsonify({ "error": "User name already in use" }), 400

    if dbLogin.login.insert_one(user):
      return self.start_session(user)
    return jsonify({ "error": "Signup failed" }), 400
  
  def signout(self):
    session.clear()
    return redirect('/')
  
  def login(self):

    user = dbLogin.login.find_one({
      "user": request.form.get('user')
    })

    if user and pbkdf2_sha256.verify(request.form.get('password'), user['password']):
      return self.start_session(user)
    
    return jsonify({ "error": "Invalid login credentials" }), 401
