import socket			
import time
import datetime
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from scipy.signal import argrelextrema
import math
import time
import select
#global xs, ys, values to plot
#global x is for 100 elements
#global buffer t
global_t = 0
scale_t = 20
sampling = 100


ecg_threshold = 65
R_peak = False
R_interval = 0
BPerM = 0
HrV = 0
xs = []
ys = []

global x
x = 100

HOST = ''
port = 12345

def socket_connect_handler(port):
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    print ("Socket successfully created")
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((HOST, port))
    #s.settimeout(20)
    print ("socket binded to %s" %(port))
    return s

def socket_message_handler(socket):
    socket.listen(5)
    print ("socket is listening")
    client , address = socket.accept()
    print ('Got connection from', address)
    #while True:
        #global_data_rv = client.recv(1000).decode('utf-8')
        #print(global_data_rv)
        #if(global_data_rv == "END"):
            #break
    return client
    
def animate(i, xs, ys, client):
    #Receive Data
    global R_interval
    global BPerM
    global R_peak_find
    global R_peak
    #global HrV
    client_available = select.select([client], [], [], 5)
    if not(client_available[0]):
        ani.event_source.stop() 
        global_t = 0
        R_interval = 0
        R_peak_find = 0
        BPerM = 0
        plt.cla(), plt.clf(), plt.close()
        xs.clear(), ys.clear()
        client.close()
        return
    global_data_rv = client.recv(scale_t*3).decode('utf-8').strip()
    #End real-time plot
    if global_data_rv.find("END") != -1:
        ani.event_source.stop() 
        global_t = 0
        R_interval = 0
        R_peak_find = 0
        BPerM = 0
        #HrV = 0
        plt.cla(), plt.clf(), plt.close()
        xs.clear(), ys.clear()
        client.close()
        return
    # print(f"Before {global_data_rv}")
   
    global_data_rv = global_data_rv.split("\n")[:scale_t]
    
    #global_data_rv = [k.strip('') for k in global_data_rv]
    global_data_rv = [k for k in global_data_rv if k.strip()]
    #print(f"List before {global_data_rv}")
    #global_data_rv = [int(k) for k in global_data_rv[:x]]
    global_data_rv = [eval(k) for k in global_data_rv]
    #print(f"length: {len(global_data_rv)}")
    #for k in range(len(global_data_rv)):
    #    if (global_data_rv[k] >=ecg_threshold):
    #        if R_peak == False:
    #            R_peak_find = len(global_data_rv) - k
    #            R_peak = True
    #        else:
    #            R_peak_find = k
    #            R_interval += R_peak_find
    #            BPerM = int(R_interval*60/sampling)
    #            #HrV += math.sqrt()
    #            R_interval = 0
    #            R_peak_find = len(global_data_rv) - k
    #            R_peak = False
    #        break
    #    else:
    #        R_peak_find = len(global_data_rv)
    #R_interval += R_peak_find
    
    #print(f"i = {i}")
    #if(len(global_data_rv) < scale_t):
    #    global_data_rv.append('99')
    #print(f"List {global_data_rv}")
    #print(f"Length= {len(global_data_rv)}")
    y = len(global_data_rv)*i
    #y = y + len(global_data_rv[:x])
    #print(f"y = {y}")
    
    global_t = i*scale_t/sampling
    #print(f"time {global_t}")
    #Time scaling
    #x_time = list(range(y, y+x))
    #x_time = [i/x for i in x_time]
    x_time = list(range(y, y+len(global_data_rv)))
    #x_time = list(range(y, y+x))
    
    #print(f"x_time = {x_time}")
    #x_time = [k/x/scale_t for k in x_time]
    x_time = [k/sampling for k in x_time]
    #print(f"x_time {x_time}")
    #ecg_data = global_data_rv
    ecg_data = [k*1.15/35 for k in global_data_rv]
    
    xs = x_time
    ys = ecg_data
    #xs.append(x_time)
    #ys.append(global_data_rv)
    
    #ax.plot(xs[i], ys[i], 'r')
    ax.plot(xs, ys, 'b')
    #ax.axes.set_xlim((-5+global_t, global_t))
    ax.axes.set_xlim((y-6*sampling)/sampling, y/sampling)

    # Format plot
    plt.xticks(rotation=45, ha='right')
    plt.subplots_adjust(bottom=0.30)
    #plt.title(f'ECG BPM: {BPerM}') 
    plt.title('ECG')
    
    
if __name__ == '__main__':
    socket = socket_connect_handler(port)
    while(True):
        client = socket_message_handler(socket)
        client.setblocking(0)
        
        fig = plt.figure(figsize=(20,20))
        ax = fig.add_subplot(1, 1, 1)
        #Plot real-time data
        time.sleep(1)
        ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys, client), cache_frame_data = False, blit=False)
        #Fix axis for plotting
        #ax.axes.set_ylim(0, 100)
        ax.axes.set_ylim(0, 3.3)
        plt.grid()
        plt.show()