from flask import Flask, render_template, flash, redirect, url_for, request, Response, current_app
#from flask_mongoengine import MongoEngine  # ModuleNotFoundError: No module named 'flask_mongoengine' = (venv) C:\flaskmyproject>pip install flask-mongoengine
from werkzeug.utils import secure_filename
import os
#from bson.json_util import dumps
import codecs
import bson.json_util
import urllib.request
import json
import io
from io import BytesIO
from function_handler import *
from pymongo import MongoClient
from bson.objectid import ObjectId
from pymongo import MongoClient
import matplotlib.pyplot as plt
import datetime
import re
import matplotlib
matplotlib.use('SVG')

mongo_client = MongoClient('mongodb+srv://khanh:duykhanh1703@cluster0.axtxkft.mongodb.net/?retryWrites=true&w=majority')
# create new database and collection instance

app = Flask(__name__, static_url_path='/')
app.secret_key = "caircocoders-ednalan-2020"

global_id = ''

@app.route('/404.html')
def error():
    return render_template('404.html')

@app.route('/')
@app.route('/index_senior.html')
def homepage():
    return render_template('index_senior.html')

@app.route('/document_list.html')
def document_list():
    # Retrieving all document IDs from MongoDB
    db = mongo_client['ecg']
    collection = db['test']
    document_ids = [str(document['_id']) for document in collection.find({}, {'_id'})]
    # Generating URLs
    #print(document_ids)
    #urls = [url_for('document_list', id=document_id) for document_id in document_ids]
    urls = [document_id for document_id in document_ids]
    # Passing URLs to the template
    return render_template('document_list.html', document = document_ids, urls=urls)

@app.route('/download/<id>') # use
def download_document(id):
    db = mongo_client['ecg']
    collection = db['test']
    # Fetch document using id
    document = collection.find_one({'_id': ObjectId(id)})

    try:
        # Convert document to JSON format
        json_data = bson.json_util.dumps(document)
        # Create a file-like object from the string data
        file = io.StringIO(json_data)
        # Create a response object
        response = Response(file.read(), mimetype='application/json')
        response.headers.set('Content-Disposition', 'attachment', filename=f'{id}.json')
        return response
    except:
        # Handle document not found error
        return 'Document not found', 404

@app.route('/pipe', methods=["GET", "POST"])
def pipe():
    global global_id
    try:
        db = mongo_client['ecg']
        collection = db['test']
        document = collection.find_one({'_id': ObjectId(global_id)})
        return {"data":document["data"], "time":document["time"], "duration":document["duration"]}
    except:
        return render_template("404.html")

@app.route('/document_list/<string:id>')
def document_plot(id):
    global global_id
    global_id = id
    # Retrieving all document IDs from MongoDB
    db = mongo_client['ecg']
    collection = db['test']
    document = collection.find_one({'_id': ObjectId(id)})
    return render_template('document_visualize.html', document = document)


@app.route('/service.html')
def service():
    return render_template('service.html')

@app.route('/blog-single.html')
def blog():
    return render_template('blog-single.html')

@app.route('/contact.html')
def contact():
    return render_template('contact.html')

@app.route('/about.html')
def about():
    return render_template('about.html')

@app.route('/handle_post', methods=['POST'])
def handle_post():
    #mongoData: "time" (string), "duration": (int), "ecg": (array of int)
    #Initalize "time" (string) , "ecg" (array of int)
    mongoData = {}
    time_now = datetime.datetime.now()
    time_now_string = time_now.strftime("%H:%M:%S")
    
    mongoData["user"] = "khanh"
    mongoData["data"] = []
    
    #Receive data
    #data = request.get_data().decode("utf-8")
    data = request.get_data()
    #print(data)
    data = data.decode("utf-8")
    if len(data) < 100:
        return "NO DATA"
    #print(f"RAW = {data}")
    #Convert data into list of int
    #data.strip()
    else:
        data = data.split("\n")
        data = [element.replace('\r', '') for element in data[:len(data)-1]]
    
        #print(f"After = {data}")
        mongoData["data"] = list(map(int,data))
        mongoData["duration"] = len(mongoData["data"])/250
        duration_string = convert_hh_mm_ss_handler(int(mongoData["duration"]))
        mongoData["time"] = time_now.strftime("%A %d/%m/%Y ") + get_init_time_mongo_handler(time_now_string,duration_string)
        #print(f"After = {mongoData["ecg"]}")
    
        #mongoJsonData = json.dumps(mongoData, indent = 4) 
    
        db = mongo_client['ecg']
        collection = db['test']
        result = collection.insert_one(mongoData)
        #print(mongoData)
        return "Message ok" 


if __name__ == '__main__':
    app.run(host='0.0.0.0', port= 8090, debug= True, threaded=True)