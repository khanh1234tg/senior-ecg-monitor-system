import datetime
import pymongo

##Convert second to hh:mm:ss
def convert_hh_mm_ss_handler(sec):
    hh_mm_ss = str(datetime.timedelta(seconds=sec))
    return hh_mm_ss

def get_database_handler(database_name, collection_name):
    # Provide the mongodb atlas url to connect python to mongodb using pymongo
    CONNECTION_STRING = "mongodb+srv://khanh:duykhanh1703@cluster0.axtxkft.mongodb.net/?retryWrites=true&w=majority"
    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    client = pymongo.MongoClient(CONNECTION_STRING)
    # Create the database for our example (we will use the same database throughout the tutorial
    return client[database_name][collection_name]

def get_init_time_mongo_handler(time, duration):
    
    duration = datetime.datetime.strptime(duration, '%H:%M:%S')
    time = datetime.datetime.strptime(time, '%H:%M:%S')
    return str(abs(time - duration))


def convert_string_list_int_handler(source):
    #convert source string into list int
    source = source.strip()
    source = source.split("\n")[:-1]
    dest = list(map(int,source))
    return dest