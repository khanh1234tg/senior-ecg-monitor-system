# DESIGN AND DEVELOPMENT OF REAL TIME ELECTROCARDIOGRAM MONITORING SYSTEM
## Project Detail
- This is a senior (pre-thesis) project required for Eletrical Engineering degree from International University (IU-VNU).
- **Author**: __Tran Duy Khanh__
- **Period**: 09/15/2023 - 01/16/2024
- **Project objective**: remote healthcare system which can monitor patients' real time heart activities ![(ECG)](https://en.wikipedia.org/wiki/Electrocardiography) at home.
- **Project outcome**: the outcome consists of two parts: 
   + Wearble ECG recording device
   + Local servers ((1)monitor in real-time and (2) provide websites))
## Development of **Wearable ECG Recording Device**
- The development consists of hardware and software phases
   + **Hardware overview**
 ![Hardware overview](./result/HardwareOverview.jpg)
- **Hardware Requirements**: 
   + MCU: [STM32F103C8T6](https://www.st.com/en/microcontrollers-microprocessors/stm32f103c8.html) (Main MCU), [ESP-12F]() (Transfer data MCU)
   + Sensor: [AD8232](https://www.analog.com/en/products/ad8232.html)
   + Regulator + Charger: [RT9013-33GB](https://www.mouser.vn/ProductDetail/Richtek/RT9013-33GB?qs=amGC7iS6iy%2FHaDkSgZH71g%3D%3D) [MCP73831T](https://www.mouser.vn/ProductDetail/Microchip-Technology/MCP73831T-2ACI-OT?qs=yUQqVecv4qvbBQBGbHx0Mw%3D%3D)
   + Interface: [SSD1306](https://cdn-shop.adafruit.com/datasheets/SSD1306.pdf), microSD card slot, buttons, LEDs
- **PCB schematics**: The schematics of components can be view in the list below, these schematics are designed with default parameters recommended by datasheets, chosen resistors and capacitors are __SMD 0603__
   + [MCU schematic](./result/StmEspSchematic.jpg)
   + [Power schematic](./result/powerSchematic.jpg)
- The outcome PCB has size of 4.1x3.8 cm2 dimesion, the trace width is 0.2 mm, hole size 0.3 mm
 ![PCB overview](./result/PCBOverview.jpg)
 ![PCB 2D](./result/PCB_2DView.jpg)
- **PCB outcome**: 
   + Early ECG PCB:
 ![ECG Early](./result/ecgEarly.jpg)
   + Complete wearable ECG device:
 ![ECG Device 1](./result/ecgDevice1.png)
 ![ECG Device 2](./result/ecgDevice2.png)

